# TuCSoN

### Important remarks

* You can start a TuCSoN **node** by running:

    ```bash
    ./gradlew tucson
    ```
  
    this will start a service listening on port 20504.
    
    * You can specify another port by starting the node as follows:
        
        ```bash
        ./gradlew tucson -Pport="port here"
        ```

* Always remember to run:

    ```bash
    ./gradlew --stop
    ```

    after each demo is completed, in order to ensure all TuCSoN nodes are actually shutdown.

* You can start a TuCSoN **inspector** by running:

    ```bash
    ./gradlew inspector
    ```
  
    A GUI application will start and you will be able to select the actual node and tuple centre to inspect

* You can always start a TuCSoN CLI (Command Line Interface) by running:

    ```bash
    ./gradlew cli -Pport=<node port>
    ```
  
    The `-Pport=<node port>` part is optional, and the `port` property defaults to `20504`.

## Example 1 -- TuCSoN Basics (`it.unibo.tucson.helloWorld` package)

0. Start a new TuCSoN node, a new inspector, on two different shells

    ```bash
    ./gradlew tucson
    ./gradlew inspector
    ```

0. On some third shell, run the `t.unibo.tucson.helloWorld.HelloWorld` class:

    ```bash
    ./gradlew helloWorld
    ```
   
   It consists of a class exploiting the TuCSoN API to interact with the aforementioned TuCSoN service
   
0. Usually the TuCSoN API is not used directly. It is aimed at letting TuCSoN agents interact over the network.
Consider for instance the `t.unibo.tucson.helloWorld.HelloWorldAgent` class, which extends `AbstractTucsonAgent`
    
    ```bash
    ./gradlew helloWorld
    ```
