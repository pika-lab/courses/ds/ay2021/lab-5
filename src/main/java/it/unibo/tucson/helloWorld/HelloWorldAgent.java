package it.unibo.tucson.helloWorld;


import alice.tuple.logic.LogicTuple;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.acc.NegotiationACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;

/**
 * Java TuCSoN Agent extending alice.tucson.api.TucsonAgent base class.
 *
 * @author s.mariani@unibo.it
 */
/*
 * 1) Extend alice.tucson.api.TucsonAgent base class.
 */
public class HelloWorldAgent extends AbstractTucsonAgent<EnhancedSyncACC> {

    private static final long DEFAULT_TIMEOUT = Long.MAX_VALUE;

    /*
     * 2) Choose one of the given constructors.
     */
    public HelloWorldAgent(String aid) throws TucsonInvalidAgentIdException {
        super(aid);
    }

    /**
     * @param args the name of the TuCSoN coordinable (optional).
     */
    public static void main(String[] args) throws TucsonInvalidAgentIdException {
        String aid = null;
        if (args.length == 0) {
            aid = "helloWorldAgent";
        } else {
            aid = args[0];
        }
        /*
         * 10) Instantiate your agent and 11) start executing its 'main()' using
         * method 'go()'.
         */
        new HelloWorldAgent(aid).go();
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        /*
         * 4.1) Get a meta ACC
         */
        NegotiationACC metaACC = TucsonMetaACC.getNegotiationContext(getTucsonAgentId());
        /*
         * 4.2) Acquire an ACC using the default Role
         */
        return metaACC.playDefaultRole();
    }

    /*
     * 3) To be overridden by TuCSoN programmers with their agent business logic.
     */
    @Override
    protected void main() throws Exception {
        /*
         * 4) Acquire an ACC
         */
        EnhancedSyncACC acc = getACC();
        /*
         * 5) Define the tuplecentre target of your coordination operations.
         */
        TucsonTupleCentreId tid = TucsonTupleCentreId.of("default", "localhost", "20504");
        /*
         * 6) Build the tuple e.g. using TuCSoN parsing facilities.
         */
        LogicTuple tuple = LogicTuple.parse("hello(world)");
        /*
         * 7) Perform the coordination operation using the preferred coordination primitive.
         */
        TucsonOperation op = acc.out(tid, tuple, DEFAULT_TIMEOUT);
        /*
         * 8) Check requested operation success.
         */
        LogicTuple res = null;
        if (op.isResultSuccess()) {
            say("Operation succeeded.");
            /*
             * 9) Get requested operation result.
             */
            res = op.getLogicTupleResult();
            say("Operation result is " + res);
        } else {
            say("Operation failed.");
        }
        /*
         * Another success test to be sure.
         */
        LogicTuple template = LogicTuple.parse("hello(Who)");
        op = acc.rdp(tid, template, DEFAULT_TIMEOUT);
        if (op.isResultSuccess()) {
            res = op.getLogicTupleResult();
            say("Operation result is " + res);
        } else {
            say("Operation failed.");
        }
        /*
         * ACC release is automatically done by the TucsonAgent base class.
         */
    }

}
